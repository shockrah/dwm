Shockrah's Build of dwm - dynamic window manager
============================
dwm is an extremely fast, small, and dynamic window manager for X.


Patches / Changes
-------
Changes: I did change the default font to Fixedsys excelsior along with the default
modkey to super. Finally some basic color changes to the bar.

Patches: By default I don't have any patches applied so they must be applied.
All patches can be found under the patches/ directory 

> Useless Gaps : https://dwm.suckless.org/patches/uselessgap/
This patch never fully works on its own via git apply so just add 
this line to your config.def.h:

```
static const unsigned int gappx     = 6;        /* gaps between windows */
```

> Movestack

Used for moving things around the window stack, which is honestly extremely useful.

Screenshot
----------

![screenshot](/ss.png)

Requirements
------------
In order to build dwm you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (dwm is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install dwm (if
necessary as root):

```
    make clean install
```


Running dwm
-----------
Add the following line to your .xinitrc to start dwm using startx:

    exec dwm

In order to connect dwm to a specific display, make sure that
the DISPLAY environment variable is set correctly, e.g.:

    DISPLAY=foo.bar:1 exec dwm

(This will start dwm on display :1 of the host foo.bar.)

In order to display status info in the bar, you can do something
like this in your .xinitrc:

```
    while xsetroot -name "`date` `uptime | sed 's/.*,//'`"
    do
    	sleep 1
    done &
    exec dwm
```


Configuration
-------------
The configuration of dwm is done by creating a custom config.h
and (re)compiling the source code.
